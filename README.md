#Observium Client Installer
##Ryan Hunter <rhunter@etiometry.com>

Contains a bash script that configures a client OS with SNMP to be used by observium to monitor the server

Currently supports Debain only, ubuntu server support soon to come

The install script currently only installs the SNMP configuration. The unix agent must be installed manually for the time being. For more information see:

https://etiometry.atlassian.net/wiki/x/GoAHAQ

##Usage

To run the installer simply run ./install as root
